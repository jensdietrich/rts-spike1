#!/usr/bin/env bash
export RTS1_HOME = $PWD
echo 'running doop analysis for the entire application with the test-all driver, context-insensitive'
cd ${DOOP_HOME}
./doop -a context-insensitive -main rts1spike.DriverForAll -i ${RTS1_HOME}/target/drivers.jar ${RTS1_HOME}/target/tests.jar ${RTS1_HOME}/target/services.jar
grep "rts1spike" ${DOOP_HOME}/last-analysis/CallGraphEdge.csv > ${RTS1_HOME}/results/cg-all-insensitive.csv
rm tmp.csv
