#!/usr/bin/env bash
export RTS1_HOME = $PWD
echo 'running doop analysis for the entire application with the test-all driver, callsite-sensitive'
cd ${DOOP_HOME}
./doop -a 1-call-site-sensitive+heap -main rts1spike.DriverForAll -i ${RTS1_HOME}/target/drivers.jar ${RTS1_HOME}/target/tests.jar ${RTS1_HOME}/target/services.jar
grep "rts1spike" ${DOOP_HOME}/last-analysis/CallGraphEdge.csv > ${RTS1_HOME}/results/cg-all-callsitesensitive.csv
rm tmp.csv
