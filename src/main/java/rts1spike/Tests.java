package rts1spike;

public class Tests {

    public static void test1(){
        invokeService(new ServiceProvider1());
    }

    public static void test2(){
        invokeService(new ServiceProvider2());
    }

    private static void invokeService(Service service) {
        service.foo();
    }

}
