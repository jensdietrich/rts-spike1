#!/usr/bin/env bash
export RTS1_HOME = $PWD
echo 'running doop analysis for the entire application with a driver for test1, context-insensitive, with the service provided by a library'
cd ${DOOP_HOME}
./doop -a context-insensitive -main rts1spike.Driver1 -i ${RTS1_HOME}/target/drivers.jar ${RTS1_HOME}/target/tests.jar -l ${RTS1_HOME}/target/services.jar
grep "rts1spike" ${DOOP_HOME}/last-analysis/CallGraphEdge.csv > ${RTS1_HOME}/results/cg-test1-insensitive-lib.csv
rm tmp.csv
